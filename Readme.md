# Sparse Model

The objective of this project is to convert and validate a provided Matlab code
for PENDANTSS (PEnalized Norm-ratios Disentangling Additive Noise, Trend and
Sparse Spikes). As its name suggests, this method allows the removal of both
noise and trend from a signal. It also allows the deconvolution of the signal. It is
useful for signals with rare peaks that carry significant information.



